import React from 'react'

const tabela = () => {
    return (
        <div className="parent">
            <h1>Tabela IMC:</h1>
            <table className="tabela">
                <tr>
                    <td className="titulo">IMC</td>
                    <td className="titulo">Classificação</td>
                </tr>

                <tr className="magreza">
                    <td>Menor que 18.5</td>
                    <td>Magreza</td>

                </tr>

                <tr className="pesoNormal">
                    <td>Entre 18.5 e 24.9</td>
                    <td>Peso Normal</td>
                </tr>

                <tr className="sobrepeso">
                    <td>Entre 25.0 e 29.9</td>
                    <td>Sobrepeso</td>
                </tr>

                <tr className="obesidade">
                    <td>Entre 30.0 e 39.9</td>
                    <td>Obesidade</td>
                </tr>

                <tr className="obesidadeGrave">
                    <td>Maior que 40.0</td>
                    <td>Obesidade Grave</td>
                </tr>
            </table>

            <footer>
                Desenvolvido por &copy; Christian Silveira
            </footer>
        </div>
    )
}

export default tabela