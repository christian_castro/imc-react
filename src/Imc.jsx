import React from 'react'

class Imc extends React.Component {
    constructor(props) {
        super(props)

        this.state = {peso: "", altura: ""}
    }

    firstInput = (evt) => {
        this.setState({peso: evt.target.value})
    }

    secondInput = (evt) => {
        this.setState({altura: evt.target.value})
    }

    Calcular = () => {
        const firstEntrance = Number(this.state.peso)
        const secondEntrance = Number(this.state.altura)
        const soma = (firstEntrance  / (secondEntrance * secondEntrance)).toFixed(2)
        this.state.peso === "" || this.state.altura === "" ? alert('Campos vazios') : this.setState({ soma })
     
    }
    
    Limpar = () => {
        this.setState({peso: "", altura: "", soma: ""}) 
    }

    render() {
      return (
        <div className="parent">

            <h1>Calculadora IMC</h1>

            <input className="inputTexto"type="number" value={this.state.peso} placeholder="Digite seu peso" onChange={this.firstInput} /> 

            <input className="inputTexto" type="number" value={this.state.altura} placeholder="Digite sua altura" onChange={this.secondInput} />

            <button className="buttonCalcular" onClick={this.Calcular}>Calcular</button>

            <button className="buttonLimpar" onClick={this.Limpar}>Limpar</button><br/>

            <div className="result-imc">
                <h2>Seu imc é: <strong className="result-imc-strong">{this.state.soma}</strong></h2>
            </div>
                
            
            </div>
        )
    }
}


export default Imc