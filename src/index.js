import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Imc from './Imc.jsx'
import TabelaImc from './TabelaImc'

ReactDOM.render(
  <div>
    <Imc />
    <TabelaImc />
  </div>,
  document.getElementById('root')
);

